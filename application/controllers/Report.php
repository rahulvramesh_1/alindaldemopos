<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
    {
           // Call the CI_Model constructor
        parent::__construct();
	   $this->load->model('Report_model');
	   $this->load->library('parser');
	}

	public function index()
	{
		$this->load->view('common/header.php');
		$this->load->view('report/index.php');
		$this->load->view('common/footer.php');
	}

	public function View()
	{
		$date = $this->input->get('date');
		$data['entries'] = $this->Report_model->getDailyReport($date);
		$this->load->view('common/header.php');
		$this->parser->parse('report/view.php', $data);
		$this->load->view('common/footer.php');
		
	}


}