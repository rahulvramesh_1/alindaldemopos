<div class="ui small modal">
  <i class="close icon"></i>
  <div class="header">
    New Customer
  </div>
  <div class="content">
   <form class="ui customer_form form">
  <div class="field">
    <label>Customer Name</label>
    <input type="text" name="customer_name" placeholder="Full Name">
  </div>
  <div class="field">
    <label>Phone Number</label>
    <input type="text" name="customer_phone" placeholder="Phone Number">
  </div>
   <div class="field">
    <label>Address</label>
    <textarea name="customer_address"></textarea>
  </div>

  </div>
  <div class="actions">
   <button class="ui green submit button">Approve</button>
    <div class="ui cancel button">Cancel</div>
  </div>
  </form>
</div>

<script type="text/javascript">
  ;$(function(){
    $('.ui.small.modal') .modal({blurring: true}).modal('attach events', '.new.customer', 'show');
     $('.customer_form')
      .form({
        on: 'blur',
        fields: {
          customer_name: {
            identifier  : 'customer_name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a value'
              }
            ]
          },
          customer_phone: {
            identifier  : 'customer_phone',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a value'
              }
            ]
          },
          customer_address: {
            identifier  : 'customer_address',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a value'
              }
            ]
          }
        },
        onSuccess: function(){
           $form  = $(this);
           // $_form = $(this).find('.billing');
          $form.addClass('loading');

           var data =  $form.serialize();
                
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/Customers/SaveItem",
                    data: data,
                    dataType: 'json'
                  })
                  .done(function(data){
                      $form.removeClass('loading');
                      if(data == 1) {
                         $('.ui.small.modal').modal('hide');
                         location.reload();
                      }
                      else{
                          $form.form('add errors', data.errors);
                          $form.find('.ui.error.message').show();
                      }
                  })
                  .fail(function( jqXHR, textStatus ) {
                      $form.removeClass('loading');
                      $form.form('add errors',['Error']);
                      $form.find('.ui.error.message').show();
                  }
                );

               return false;
        }

      });
  });
</script>