<!DOCTYPE html>
<html>
<head>
 <title>Alindal POS</title>
 <script type="text/javascript" src="<?php echo base_url()."theme/jquery-3.1.0.min.js"; ?>"></script>
 <script type="text/javascript" src="<?php echo base_url()."theme/semantic.js"; ?>"></script>
 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>

 <link rel="stylesheet" type="text/css" href="<?php echo base_url()."theme/semantic.css"; ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."theme/theme.css"; ?>">
    <style type="text/css">
  body {
    background-color: #FFFFFF;
  }
  .ui.menu .item img.logo {
    margin-right: 1.5em;
  }
  .main.container {
    margin-top: 7em;
  }
  .wireframe {
    margin-top: 2em;
  }
  .ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
  }
  </style>

</head>
<body>
<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";
</script>
 <div class="ui fixed inverted menu">
    <div class="ui container">
      <a href="#" class="header item">
        <img class="logo" src="http://semantic-ui.com/examples/assets/images/logo.png">
        Alindal Billing
      </a>
      <a href="<?php echo site_url('Billing')?>" class="item">Home</a>
      <a href="#" class="item new customer">New Customer</a>
      <a href="<?php echo site_url('Customers')?>" class="item new ">List Customers</a>
      <a href="<?php echo site_url('Report')?>" class="item ">Daily Report</a>
      <!-- <div class="ui simple dropdown item">
        Menu <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="#">Customers</a>
          <a class="item" href="#">Billing</a>
          <div class="divider"></div>
          <div class="header">Header Item</div>
          <div class="item">
            <i class="dropdown icon"></i>
            Sub Menu
            <div class="menu">
              <a class="item" href="#">Link Item</a>
              <a class="item" href="#">Link Item</a>
            </div>
          </div>
          <a class="item" href="#">Link Item</a>
        </div>
      </div> -->
    </div>
  </div>

