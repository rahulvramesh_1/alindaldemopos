<div class="ui main container">
	<h2 class="ui dividing header">Billing</h2>
	<div class="ui segment" style="min-height: 8rem;">
	<h4 class="ui dividing header">Customer Info</h4>

	<form class="ui form billing">
	 <div class="field">
	    <label>Select Customer</label>
	    <select class="ui search dropdown" name="customer_name">
	      <option value="">Select Customer / Phone Number</option>
	      <?php foreach ($customers as $customer) : ?>
	      	
	      <option value="<?php echo $customer->id; ?>"><?php echo $customer->name; ?> - <?php echo $customer->phone; ?></option>

	     <?php endforeach; ?>
	    </select>
	  </div>

	<!--   <div class="ui middle aligned divided list ">
		  <div class="item">
		    <div class="right floated content">
		      <div class="ui red button">Remove</div>
		    </div>
		    <img class="ui avatar image" src="http://semantic-ui.com/images/avatar2/small/molly.png">
		    <div class="content">
		      <a class="header">Daniel Louise</a>
		      <div class="description">Last seen watching <a><b>Arrested Development</b></a> just now.</div>
		    </div>
		  </div>
		  
		</div>
 -->
      <h4 class="ui dividing header">Billing Information</h4>
     
      <div class="five fields">
        <div class="field">
          <label>Product name</label>
          <input type="text" placeholder="Product name" name="product_name">
        </div>
        <div class="field">
          <label>Product Code</label>
          <input type="text" placeholder="Product Code" name="product_code">
        </div>
        <div class="field">
          <label>Quantity</label>
          <input type="text" placeholder="Quantity" name="product_quantity">
        </div>
        <div class="field">
          <label>Total</label>
          <input type="text" placeholder="Total" name="product_total">
        </div>
         <div class="field">
         <br/>
         <button class="ui labeled icon button" style="margin-top: 4px;">
		  <i class="add icon"></i>
		  Add To Bill
		</button>
        </div>
      </div>
    </form>

     <h4 class="ui dividing header">Invoice Preview</h4>
	 <div class="ui one column grid">
	 <div class="column" id="invoice_preview">
	 

	  

  		</div>
	 </div>

	 <!-- Cancel -->
	 <div class="eight column row">

	    <div class="column">
	    	<a href="<?php echo base_url(); ?>index.php/Billing/DiscardInovice"><button class="ui button">
			  Cancel
			</button></a>

			<a href="<?php echo base_url(); ?>index.php/Billing/Invoice"><button class="ui red button">
			  Print
			</button></a>
	    </div>
	    
	  </div>

	 
	</div>
</div>

<script type="text/javascript">
	;$(function() {
  	  $('.billing')
		  .form({
		    on: 'blur',
		    fields: {
		      product_name: {
		        identifier  : 'product_name',
		        rules: [
		          {
		            type   : 'empty',
		            prompt : 'Please enter a value'
		          }
		        ]
		      },
		      product_code: {
		        identifier  : 'product_code',
		        rules: [
		          {
		            type   : 'empty',
		            prompt : 'Please enter a value'
		          }
		        ]
		      },
		      product_quantity: {
		        identifier  : 'product_quantity',
		        rules: [
		          {
		            type   : 'empty',
		            prompt : 'Please enter a value'
		          }
		        ]
		      },
		      product_total: {
		        identifier  : 'product_total',
		        rules: [
		          {
		            type   : 'empty',
		            prompt : 'Please enter a value'
		          }
		        ]
		      },
		      customer_name: {
		        identifier  : 'customer_name',
		        rules: [
		          {
		            type   : 'empty',
		            prompt : 'Please enter a value'
		          }
		        ]
		      }
		    },
		    onSuccess: function(){
		    	 $form  = $(this);
		    	 // var data = $form.serialize();
		    	 $.post( base_url+"index.php/Billing/AddItem", $form.serialize())
				  .done(function( data ) {
				     $.get( base_url+"index.php/Billing/GetProductsView", function( data ) {
					  $( "#invoice_preview" ).html( data );
					});
				  });
		    	 // // $_form = $(this).find('.billing');
        //   		 $form.addClass('loading');

        //   		   var data =  $form.serialize();
          		  
	       //    		$.ajax({
			     //          type: "POST",
			     //          url: "Billing/AddItem",
			     //          data: data,
			     //          dataType: 'json'
			     //        })
			     //        .done(function(data){
			     //           alert("Test");
			     //        })
			          //   .fail(function( jqXHR, textStatus ) {
			          //       // $form.removeClass('loading');
			          //       // $form.form('add errors',['Error']);
			          //       // $form.find('.ui.error.message').show();
			          //   }
			          // );

        		 return false;
		    }

		  });

		  $('select.dropdown').dropdown();

		  $.get( base_url+"index.php/Billing/GetProductsView", function( data ) {
			  $( "#invoice_preview" ).html( data );
			});

	});
</script>